#!/bin/sh

echo "Automatic commit..."

alias CP='/usr/bin/rsync --verbose --times --perms --compress --human-readable --progress --archive'

#markdown README.md > index.html && \
markdown README.md > README.html && \
#echo "index.html: generated....."

rm -f .*~ *~ */*~ */.*~ *.py[co] */*.py[co] && \
echo "Temp files: deleted....."

CP -r ./ besson@zamok.crans.org:~/www/publis/gedit-tools/ && \
echo "Syncr with Zamok: done....."

CP -r ./ lbesson@ssh.dptinfo.ens-cachan.fr:~/public_html/publis/gedit-tools/ && \
echo "Syncr with Zamok: done....."

git add "$0" *.gedit-tools README.md && git commit -m "Auto commit, with $0..... \n$@" \
 && git push && echo "Auto commit: done....."

echo "Done..."
