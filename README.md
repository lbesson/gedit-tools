Gedit Tools : for "External Tools" plugin
=========================================

This *git* repository hosts a few **scripts** to enhance **gedit**, the amazing Gnome Text Editor.

Requirements:
-------------

Those *scripts* are made to be used with [**gedit**](<http://projects.gnome.org/gedit/> "official homepage for the Gedit project"), the text editor of the *Gnome* project.  
It **requires** the *plugin* "External Tools".
The reference page is [here](<https://live.gnome.org/Gedit/Plugins/ExternalTools> "take a look !").  
Usually, this *plugin* is installed by default on all major Linux distribution that have **gedit** pre-installed.

I developped those tools for **gedit** version 3.2.3. 
This project will work with newer version of this software, 
but **I cannot ensure** that those script will also work with *previous* version of **gedit**.

#### Warning:
 On Microsoft Windows, or on Mac OS X, all those scripts might be useless, because this plugin is unavailable.  
 *But, who use those OSs ;) ?*

----

How to use those scripts
-------------------------
 
 0. Open a terminal (``Ctrl+Alt+T`` on Ubuntu);

 1. Clone this *git* repository (this require an active *Internet* connection) :
    
        git clone git@bitbucket.org:lbesson/gedit-tools.git
    
    You can also download it (probably a file like
    ``lbesson-gedit-tools-08505e7644a0.zip``, with a different number),
    and extract the *tarball* :
        
        unzip lbesson-gedit-tools-08505e7644a0.zip

 2. This create a *folder* named ``gedit-tools``;

 3. Go to this *folder* :
    
        cd gedit-tools/
        
    If you chosed to download the *zip* file :
        
        cd lbesson-gedit-tools-08505e7644a0/

 4. Then, copy each of the scripts you are interested in, or all, to the folder : ``~/.config/gedit/tools/`` :
 
        cp *.gedit-tools ~/.config/gedit/tools/
 
 5. Restart **gedit**, and the scripts should be available.
    
    * They are accessible through the *main menu*, in the column **Tool**, at the point **External Tools**.
    
    * If this *menu* entry is not there, the first thing to check is to see if the *plugin* is active or not.  
      This can be done with the **Preferences** menu.
    
      ![This screenshot show this preference menu](exemples/preference_menu.png "Preference Menu")  
      Be sure to check this box (sorry, the screenshot is in french).
    
    * Some are also *binded* with a keyboard shortcut,
    like ``Ctrl+Alt+Space`` or ``Ctrl+Alt+*`` for the *Interpreter* tools.
    
      You can of course *change* them, via the **Manage ExternalTools** window.
    
      ![This screenshot show shortcut menu](exemples/gestion-tools.png "Gestion Tool")
 
 6. (Optionnal) you can test the tools.
 
 7. You can delete the folder, to clean up :
 
        cd .. && rm -rf gedit-tools/
        
    If you chosed to download the *zip* file :
        
        cd .. && rm -rf lbesson-gedit-tools-08505e7644a0/




### Feel free to work on them

This project is released in the *public* domain,
so I would **love** to get some *return* from you.  
If you use one of those tools, feel free to contact me, to contribute or anything.

### About the name of the scripts

The script are all named : ``something.gedit-tools``.
This is **not** mandatory, just a convention to know what kind of script it is.

----

About:
======

#### Author:
**Lilian Besson** (for Naereen CORP.).

#### Language(s):
 Python v2.7+.
 GNU Bash 4.2+.

Plateform(s)
------------

The project have been *developped* on *GNU/Linux* (Ubuntu 11.10).

And I think it will **only** work on Linux.
   
About the project
-----------------

This project is part of my work realised to enhance **gedit**.

The first reason was because I love the visual appearance of **GTK SourceView** syntaxic coloration.
 So I started to work on this syntaxic coloration tool.
 You can check this page, which host my work on this
 [here](<https://sites.google.com/site/naereencorp/tools/gtksourceview> "On my Google Site website").

And many of my colleague are using ``Emacs``, ``Vim`` or ``Eclipse``,
and they are so proud to use those editors, they always says their is the best, and so on...

That's why I decided to try not to use one of those fancy and famous editors, and keep the one I was using since my first step on *GNU/Linux*.

And because I was curious, I worked on **GTK SourceView**, on plug-ins (mainly the embedded terminal) and on scripts for ExternalTools. 

So, the main reason I love **gedit** is because it has a very nice looking,
and it is enhanceable by *anyone*, **easily**.
 
 Or, at least, I love enhancing it...

About the doc
-------------
 
A short documentation and a few screenshots are hosted
[here on my Google Site](<https://sites.google.com/site/naereencorp/gedit> "check this out !").

#### Warning:
 By now, this doc is *in french*, sorry about that.
 I'm working on a translation...

Those documents are also hosted [here](<http://perso.crans.org/besson/publis/gedit-tools/> "on my Cr@ns web page !").

Contact me
----------

Feel free to contact me, either with a bitbucket message
(my profile is [lbesson](https://bitbucket.org/lbesson/ "here")),
or via an email at **lilian DOT besson AT ens-cachan DOT fr**.

License(s)
==========

#### Logo

The logo is from the **gedit** official homepage,
and it is released under the term of the [GPL license](<http://www.gnu.org/licenses/gpl.html> "here").

#### Scripts

This project is released under the **GPLv3 license**,
for more details, take a look at the ``LICENSE`` file in the source.
